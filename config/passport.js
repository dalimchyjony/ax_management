const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

module.exports = function(passport) {
  passport.use(
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
      // Match user
      utilsLogin.findOneEmail(email).then((result)=>{
        if(result.status){
          if(result.data != null){
            bcrypt.compare(password, result.data.password, (err, isMatch) => {
              if (err) throw err;
              if (isMatch) {
                return done(null, result.data);
              } else {
                return done(null, false, { message: 'Password incorrect.' });
              }
            });
          }else{
            return done(null, false, { message: 'That email is not registered.' });
          }
        }
      }).catch((err)=>{
        done(err, null);
      });
    })
  );

  passport.serializeUser(function(user, done) {
    done(null, user.user_id);
  });

  passport.deserializeUser(function(id, done) {
    utilsUser.findUserInfo(id).then((result)=>{
      if(result.status){
        var userData = {};
        userData['user'] = result.data
        utilsCompany.findCompanyInfo(userData.user.company_id).then(result1 =>{
          if(result1.status){
            userData['company_info'] = result1.data;
            done(null, userData);
          }
        }).catch(err1 =>{
          console.log(56,err1);
        })
      }else{
        console.log(56,result);
      }
    }).catch((err)=>{
      done(err, null);
    });
  });
};
