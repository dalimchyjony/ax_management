var express = require('express');
var router = express.Router();
const passport = require('passport');
const _ = require('loadsh');
const { forwardAuthenticated,ensureAuthenticated } = require('../config/auth');
var path = require('path');
var webp = require('webp-converter');
var multer = require('multer');
var moment = require('moment');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
      mkdirp('./public/uploads/'+req.user.user.company_id+'', function(err) { 
        if(err){
          console.log({status:false,data:err});
        }else{
          console.log({status:true});
        }
      });
      
      cb(null, path.resolve(`./public/uploads/${req.user.user.company_id}`))
  },
  filename: function (req, file, callback) {
      callback(null, file.originalname.replace(path.extname(file.originalname), '_') +Date.now() +  path.extname(file.originalname));
  }
});

const uploadFile = multer({ storage }).array('imageUpload', 1000);



var filetypecheck = (file)=>{
  var name = file.split('.')
  var type = name[name.length - 1];
  switch (type) {
      case "txt":
      case "doc":
      case "mpg":
          return "doc";
      case "jpg":
      case "gif":
      case "png":
      case "svg":
      case "jfif":
      case "webp":
          return "img";

      default:
          return "doc";
  }
}

var getOrderStatus = (type)=>{
  switch (type) {
    case 0:
        return "INITIATED";
    case 1:
        return "DELIVERED";
    case 2:
        return "RETURN";
    case 3:
        return "CANCELED";
    case 4:
        return "PENDDING";
    default:
        return "INITIATED";
  }
}

router.post('/fileupload',ensureAuthenticated, function(req, res) {
  uploadFile(req,res, function(err){
				if (req.files.length < 1) {
					res.json({ 'msg': 'No files were uploaded.' });
				} else {
          if(req.files[0].mimetype.indexOf('image/') > -1){
						var newFileName = req.files[0].filename.split('.');
						newFileName[newFileName.length - 1] = 'webp';
						newFileName = newFileName.join('.');

						webp.cwebp('./public/uploads/'+req.user.user.company_id+'/'+req.files[0].filename,'./public/uploads/'+req.user.user.company_id+'/'+newFileName,"-q 50",function(status,error)
							{
                if(error != ''){
                  var data = {
                    company_id:req.user.user.company_id,
                    file_type:filetypecheck(req.files[0].filename),
                    created_by:req.user.user.user_id,
                    file_name:req.files[0].filename,
                    req_type:'createNewfile'
                  }
                  utilsFiles.file_management(data,function(newres){
                    if(newres.status){
                      res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl,'data':newres.data  });
                    }else{
                      res.json(newFile)
                    }
                  });
                }else{

                  req.files[0].filename = newFileName;
                  var data = {
                    company_id:req.user.user.company_id,
                    file_type:filetypecheck(newFileName),
                    created_by:req.user.user.user_id,
                    file_name:newFileName,
                    req_type:'createNewfile'
                  }
                  utilsFiles.file_management(data,function(newres){
                    if(newres.status){
                      res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl,'data':newres.data });
                    }else{
                      res.json(newFile)
                    }
                  });
                }
							});
					}else{
            var data = {
              company_id:req.user.user.company_id,
              file_type:filetypecheck(req.files[0].filename),
              created_by:req.user.user.user_id,
              file_name:req.files[0].filename,
              req_type:'createNewfile'
            }
            utilsFiles.file_management(data,function(newres){
              if(newres.status){
                res.json({ 'file_info': req.files, 'msg': 'Successfully uploaded', 'sl': req.body.sl,'data':newres.data  });
              }else{
                res.json(newFile)
              }
            });
					}
				
				}
	});
});


/* GET home page. */
router.get('/', ensureAuthenticated,async function(req, res) {
  var total_order = await utilsOrders.countTotalOrderBycompany(req.user.company_info.company_id);
  var total_order_pendding = await utilsOrders.countTotalOrderPenddingBycompany(req.user.company_info.company_id);
  var total_order_delivered = await utilsOrders.countTotalOrderDeliveredBycompany(req.user.company_info.company_id);
  var total_order_return = await utilsOrders.countTotalOrderReturnBycompany(req.user.company_info.company_id);
  var total_stocks= await utilsProduct.countTotalProductBycompany(req.user.company_info.company_id);
  var total_low_stocks= await utilsProduct.countTotalLowStocksBycompany(req.user.company_info.company_id);
  var total_customer= await utilsCustomer.countTotalCustomerBycompany(req.user.company_info.company_id);

  var resdata = {
     title: 'AX Management',
     page:'dashboard',
     ses_data:req.user,
     total_order:total_order,
     total_order_pendding:total_order_pendding,
     total_order_delivered:total_order_delivered,
     total_order_return:total_order_return,
     total_stocks:total_stocks,
     total_low_stocks:total_low_stocks,
     total_customer:total_customer,
     restart_time:restart_time

  }
  res.render('ax_management',resdata );
});

/* GET register page. */
router.get('/register',forwardAuthenticated, function(req, res) {
  res.render('initial_page', { title: 'Register | AX Management',page:'register' });
});
/* GET login page. */
router.get('/login',forwardAuthenticated, function(req, res) {
  var resdata = {
    title: 'Login | AX Management',
    page:'login',
    error:res.locals.error,
    _:_,
    restart_time:restart_time
 }
  res.render('initial_page',resdata);
});
// Login
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/ax-management',
    failureRedirect: '/ax-management/login',
    failureFlash: true
  })(req, res, next);
});

// Logout
router.get('/logout', (req, res) => {
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/ax-management/login');
});

/* GET add item page. */
router.get('/add-item',ensureAuthenticated, async function(req, res) {
  var category_list = await utilsCategory.getAllCategorybycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Add Item | AX Management',
      page:'additem',
      ses_data:req.user,
      category_list:category_list.data,
      _:_,
      restart_time:restart_time
  }
  res.render('ax_management',resdata);
});
/* GET items page. */
router.get('/items',ensureAuthenticated, async function(req, res) {
var item_list = await utilsProduct.getAllProductbycompany(req.user.company_info.company_id);
var category_list = await utilsCategory.getAllCategorybycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Items | AX Management',
      page:'items',
      ses_data:req.user,
      item_list:item_list.data,
      category_list:category_list.data,
      _:_,
      restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET purchase item page. */
router.get('/purchase-item',ensureAuthenticated, async function(req, res) {
  var supplier_list = await utilsSupplier.getAllsupplierbycompany(req.user.company_info.company_id);
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var purchase_list = await utilsPurchaseProduct.getAllpurchasebycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Purchase Item | AX Management',
      page:'purchase_item',
      ses_data:req.user,
      supplier_list:supplier_list.data,
      branch_list:branch_list.data,
      purchase_list:purchase_list.data,
      moment:moment,
      _:_,
      restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET add order page. */
router.get('/add-order',ensureAuthenticated, async function(req, res) {
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Add Order | AX Management',
      page:'add_order',
      ses_data:req.user,
      branch_list:branch_list.data,
      _:_,
      restart_time:restart_time

  }
  res.render('ax_management', resdata);
});
/* GET orders page. */
router.get('/orders',ensureAuthenticated, async function(req, res) {
  var order_list = await utilsOrders.getAllOrdersListbycompany(req.user.company_info.company_id);
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Orders | AX Management',
      page:'orders',
      ses_data:req.user,
      order_list:order_list.data,
      moment:moment,
      branch_list:branch_list.data,
      _:_,
      getOrderStatus:getOrderStatus,
      restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/item-category',ensureAuthenticated, async function(req, res) {
  var category_list = await utilsCategory.getAllCategorybycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'Category | AX Management',
      page:'item_category',
      ses_data:req.user,
      category_list:category_list.data,
      _:_,
      restart_time:restart_time
  }
  res.render('ax_management', resdata);
});

/* GET item Category page. */
router.get('/user-management',ensureAuthenticated, async function(req, res) {
  var user_list = await utilsUser.getAllUserBycompany(req.user.company_info.company_id);
  var resdata = {
      title: 'User Management | AX Management',
      page:'user_management',
      ses_data:req.user,
      user_list:user_list.data,
      _:_,
      restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/supplier-management',ensureAuthenticated, async function(req, res) {
  var supplier_list = await utilsSupplier.getAllsupplierbycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Supplier Management | AX Management',
    page:'supplier_management',
    ses_data:req.user,
    supplier_list:supplier_list.data,
    _:_,
    restart_time:restart_time
  }
  res.render('ax_management',resdata);
});
/* GET item Category page. */
router.get('/branch-management',ensureAuthenticated, async function(req, res) {
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Branch Management | AX Management',
    page:'branch_management',
    ses_data:req.user,
    branch_list:branch_list.data,
    _:_,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/account-management',ensureAuthenticated, function(req, res) {
  var resdata = {
    title: 'Account Management | AX Management',
    page:'account_management',
    ses_data:req.user,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/customer-management',ensureAuthenticated, async function(req, res) {
  var customer_list = await utilsCustomer.getAllcustomerbycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Customer Management | AX Management',
    page:'customer_management',
    ses_data:req.user,
    customer_list:customer_list.data,
    _:_,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/invoice-category',ensureAuthenticated, function(req, res) {
  var resdata = {
    title: 'Invoice Category | AX Management',
    page:'invoice_category',
    ses_data:req.user,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});

/* GET item Category page. */
router.get('/create-satement',ensureAuthenticated, async function(req, res) {
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Create Satement | AX Management',
    page:'create_statement',
    ses_data:req.user,
    branch_list:branch_list.data,
    _:_,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});

router.get('/invest-withdraw',ensureAuthenticated, async function(req, res) {
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var user_list = await utilsUser.getAllUserBycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Create Satement | AX Management',
    page:'invest_withdraw',
    ses_data:req.user,
    branch_list:branch_list.data,
    user_list:user_list.data,
    _:_,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
router.get('/salary-management',ensureAuthenticated, async function(req, res) {
  var branch_list = await utilsBranch.getAllbranchbycompany(req.user.company_info.company_id);
  var user_list = await utilsUser.getAllUserBycompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Salary Management | AX Management',
    page:'salary_management',
    ses_data:req.user,
    branch_list:branch_list.data,
    user_list:user_list.data,
    _:_,
    moment:moment,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/statement-manager',ensureAuthenticated, async function(req, res) {
  var statement_list = await utilsStatement.getAllStatementByCompany(req.user.company_info.company_id);
  var resdata = {
    title: 'Statement Manager | AX Management',
    page:'statement_manager',
    ses_data:req.user,
    statement_list:statement_list.data,
    moment:moment,
    restart_time:restart_time,
    _:_
  }
  res.render('ax_management', resdata);
});
/* GET item Category page. */
router.get('/transaction',ensureAuthenticated, function(req, res) {
  var resdata = {
    title: 'Transaction | AX Management',
    page:'transaction',
    ses_data:req.user,
    restart_time:restart_time
  }
  res.render('ax_management', resdata);
});

/* GET item Category page. */
router.get('/view-invoice/:type/:id',ensureAuthenticated, async function(req, res) {
  var resdata = {
    title: 'AX Management | Invoice',
    page:'order_invoice',
    ses_data:req.user,
    moment:moment,
    restart_time:restart_time,
    _:_
  }
  if(req.params.type == 'order'){
   var order_details = await utilsOrders.get_single_order(req.params.id);
   if(order_details.status){
     if(order_details.data != null){
      resdata['order'] = order_details.data;
      res.render('templates/order_invoice', resdata);
     }
   }
  }
});

module.exports = router;
