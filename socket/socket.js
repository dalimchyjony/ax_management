var _ = require('lodash');
var moment = require('moment');

module.exports = function (io) {
  var app = require('express');
  var router = app.Router();

  io.on('connection', function (socket) {
    socket.join('1');

    socket.on('login', function (userdata) {
      socket.join('axmanagement-2019');
      socket.join(userdata.from);
      socket.handshake.session.userdata = userdata;
      socket.handshake.session.save();

      if (alluserlist.indexOf(userdata.from) != -1) {
        console.log(userdata.text + ' is connected into socket');
      } else {
        alluserlist.push(userdata.from);
        console.log(userdata.text + ' is connected into socket');
      }

      socket.emit('online_user_list', alluserlist); // this emit receive only who is login
      socket.broadcast.emit('new_user_notification', socket.handshake.session.userdata); // this emit receive all users except me
      console.log(38,alluserlist)
    });

    socket.on('disconnect', function () {
      console.log('disconnect*********************',socket.handshake.session.userdata);
      io.sockets.in('axmanagement-2019').emit('logout', { userdata: socket.handshake.session.userdata });
      if (socket.handshake.session.userdata) {
          alluserlist = alluserlist.filter(function (el) {
            return el !== socket.handshake.session.userdata.from;
          });
          delete socket.handshake.session.userdata;
          console.log(38,alluserlist)
          socket.handshake.session.save();
        }
    });

    socket.on('register',function(data,callback){
      utilsLogin.register(data,function(res){
        callback(res);
      });
    });

    socket.on('branch_management',function(data,callback){
      utilsBranch.branch_management(data,function(res){
        callback(res);
      });
    });

    socket.on('supplier_management',function(data,callback){
      utilsSupplier.supplier_management(data,function(res){
        callback(res);
      });
    });

    socket.on('user_management',function(data,callback){
      utilsUser.user_management(data,function(res){
        callback(res);
      });
    });

    socket.on('customer_management',function(data,callback){
      utilsCustomer.customer_management(data,function(res){
        callback(res);
      });
    });

    socket.on('category_management',function(data,callback){
      utilsCategory.category_management(data,function(res){
        callback(res);
      });
    });
    
    socket.on('purchase_management',function(data,callback){
      utilsPurchaseProduct.purchase_management(data,function(res){
        callback(res);
      });
    });

    socket.on('product_management',function(data,callback){
      utilsProduct.product_management(data,function(res){
        callback(res);
      });
    });

    socket.on('order_management',function(data,callback){
      utilsOrders.order_management(data,function(res){
        callback(res);
      });
    });
    socket.on('statement_management',function(data,callback){
      utilsStatement.statement_management(data,function(res){
        callback(res);
      });
    });

    socket.on('getAllFilebycompany',function(data,callback){
      utilsFiles.getAllFilebycompany(data).then(result=>{
        callback(result);
      }).catch(err=>{
        callback(err);
      })
    });

    socket.on('getAllProductbycompany',function(data,callback){
      utilsProduct.getAllProductbycompany(data).then(result=>{
        callback(result);
      }).catch(err=>{
        callback(err);
      })
    });


///end socket line
  });

  return router;
}
