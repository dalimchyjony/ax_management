var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Supplier = require('../models/supplier');
var moment = require('moment');

var supplier_management = async (data,callback)=>{
    if(data.req_type == 'createNewSupplier'){
        var newData = {
            company_id:data.company_id,
            supplier_id:uuidv4(),
            supplier_name:data.supplier_name,
            supplier_phone:data.supplier_phone,
            supplier_address:data.supplier_address,
            supplier_email:data.supplier_email,
            created_by:data.created_by,
            supplier_status:(data.supplier_status == '1')? true:false
        }
        var findSupplier = await findsupplierbyName(newData.supplier_name,newData.company_id);
        if(findSupplier.status){
            if(findSupplier.data == null){
                var newSupplider = await createNewSupplier(newData);
                if(newSupplider.status){
                    callback({status:true,data:newSupplider.data});
                }else{
                    callback({status:false,msg:newSupplider.err});
                }
            }else{
                callback({status:false,msg:'Supplier Name Already Exists'});
            }
        }else{
            callback({status:false,msg:err});
        }

        
    }
}

var createNewSupplier=(data)=>{
    return new Promise((resolve,reject)=>{
        new Supplier(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var findsupplierbyName=(supplier_name,company_id)=>{
    return new Promise((resolve,reject)=>{
        Supplier.findOne({supplier_name:supplier_name,company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var getAllsupplierbycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Supplier.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var updateSupplierAmount=(data)=>{
    return new Promise((resolve,reject)=>{
        Supplier.updateOne({company_id:data.company_id,supplier_id:data.supplier_id},{$inc: { total_amount: data.amount }},function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

module.exports = {supplier_management,getAllsupplierbycompany,updateSupplierAmount};
