var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Category = require('../models/category');
var moment = require('moment');

var category_management = async (data,callback)=>{
    if(data.req_type == 'createNewCategory'){
        var newData = {
            company_id:data.company_id,
            category_id:uuidv4(),
            category_name:data.category_name,
            parent_category_id:data.parent_category_id,
            category_slug:data.category_slug,
            created_by:data.created_by,
            category_status:(data.category_status == '1')? true:false
        }
        var find_slug = await findcategory_by_slug(newData.category_slug);
        if(find_slug.status){
            if(find_slug.data.length > 0){
                newData.category_slug = newData.category_slug+'-'+(find_slug.data.length + 1); 
            }
            var create_category = await createNewCategory(newData);
            if(create_category.status){
                callback({status:true, data:newData}); 
            }else{
                callback({status:false, msg:'Something Wrong!!!',err:create_category.data});  
            }

        }else{
            callback({status:false, msg:'Something Wrong!!!'});
        }
    }
}

var findcategory_by_slug = (category_slug)=>{
    return new Promise((resolve,reject)=>{
        Category.find({category_slug:category_slug},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
               
                resolve({status:true,data:result});
            }
        })
    })
}

var createNewCategory=(data)=>{
    return new Promise((resolve,reject)=>{
        new Category(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var getAllCategorybycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Category.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

module.exports = {category_management,getAllCategorybycompany};
