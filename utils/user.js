var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const User = require('../models/user');
var moment = require('moment');
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);



var user_management = async (data,callback)=>{
    try{

        if(data.req_type == 'createNewUser'){
            var newData = {
                company_id:data.company_id,
                user_id:uuidv4(),
                user_name:data.user_name,
                user_phone:data.user_phone,
                user_role:data.user_role,
                user_email:data.user_email,
                user_address:data.user_address,
                user_gender:data.user_gender,
                user_nid:data.user_nid,
                user_dob:data.user_dob,
                user_type:data.user_type,
                user_designation:data.user_designation,
                salary:data.salary,
                user_dob:data.user_dob,
                created_by:data.created_by,
                status:(data.status == '1')? true:false
            }
            var phone = await utilsLogin.findOnePhone(newData.user_phone);
            var email = await utilsLogin.findOneEmail(newData.user_email);
            if(phone.status && email.status){
                if(phone.data != null){
                    callback({status:false, msg:'Phone Number Already Exist.'});
                }else if(email.data != null ){
                    callback({status:false, msg:'Email Address Already Exist.'});
                }else{
                    var hash = bcrypt.hashSync('123456', salt);
                    var logindata = {
                        user_id:newData.user_id,
                        company_id:newData.company_id,
                        user_phone:newData.user_phone,
                        user_email:newData.user_email,
                        password:hash,
                        status:true,
                    }
                    var createnew = await utilsLogin.createNewLogin(logindata);
                    if(createnew.status){
                        var createuser = await createNewUser(newData);
                        if(createuser.status){
                            callback({status:true, data:newData}); 
                        }else{
                            callback({status:false, msg:'Something Wrong!!!',err:createuser.data}); 
                        }
                    }else{
                        callback({status:false, msg:'Something Wrong!!!'});
                    }
                }
            }else{
                callback({status:false, msg:'Something Wrong!!!'});
            }
            
        }else if(data.req_type == 'get_single_user'){
            var getsingledata = await findUserInfo(data.user_id)
            callback(getsingledata);
        }else if(data.req_type == 'update_user'){
            var updateUser = await updateSingeleUser(data);
            callback(updateUser);
        }else if(data.req_type == 'update_current_month'){
            var updateUser = await updateSingeleUser(data);
            callback(updateUser);
        }
    }catch(err){
        console.log(err);
    }
}

var createNewUser=(data)=>{
    return new Promise((resolve,reject)=>{
        new User(data).save(function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}


var findUserInfo=(user_id)=>{
    return new Promise((resolve,reject)=>{
        User.findOne({user_id:user_id},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}

var getAllUserBycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        User.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}

var updateSingeleUser = (data)=>{
    return new Promise((resolve,reject)=>{
        if(data.req_type == 'withdrawORinvest'){
            if(data.statement_category == 'withdraw'){
                User.updateOne({user_id:data.user_id},{$inc:{total_withdraw:parseInt(data.amount)}},function(err,result){
                    if(err){
                        console.log(115,err);
                        resolve({status:false,err:err});
                    }else{
                        resolve({status:true,data:result});
                    }
                })

            }else{
                User.updateOne({user_id:data.user_id},{$inc:{total_invest:parseInt(data.amount)}},function(err,result){
                    if(err){
                        console.log(115,err);
                        resolve({status:false,err:err});
                    }else{
                        resolve({status:true,data:result});
                    }
                })
            }
        }else if(data.req_type == 'update_user'){
            User.updateOne({user_id:data.user_id},{salary:data.salary,user_designation:data.user_designation,status:data.status,user_type:data.user_type,user_dob:data.user_dob,user_nid:data.user_nid,user_gender:data.user_gender,user_address:data.user_address,user_role:data.user_role,user_phone:data.user_phone,user_name:data.user_name},function(err,result){
                if(err){
                    console.log(115,err);
                    resolve({status:false,err:err,msg:'Some thing wrong'});
                }else{
                    resolve({status:true,data:result,msg:'Success'});
                }
            })
        }else if(data.req_type == 'update_current_month'){
            User.updateOne({user_id:data.user_id},{due_payment:data.due_payment,current_month_payment:data.current_month_payment,current_month:data.current_month},function(err,result){
                if(err){
                    console.log(115,err);
                    resolve({status:false,err:err,msg:'Some thing wrong'});
                }else{
                    resolve({status:true,data:result,msg:'Success'});
                }
            })
        }else if(data.req_type == 'salary'){
            User.updateOne({user_id:data.user_id},{$inc:{current_month_payment:data.amount},last_salary_payment:data.create_at},function(err,result){
                if(err){
                    console.log(115,err);
                    resolve({status:false,err:err,});
                }else{
                    resolve({status:true,data:result});
                }
            }) 
        }
    });
}


module.exports = {createNewUser,findUserInfo,getAllUserBycompany,user_management,updateSingeleUser};
