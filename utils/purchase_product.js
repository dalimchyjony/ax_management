var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const PurchaseProduct = require('../models/purchase_product');
var moment = require('moment');
var shortid = require('shortid');

var purchase_management = async (data,callback)=>{
    try{
        if(data.req_type == 'addNewPurchase'){
            var total_amount = (Number(data.purchase_price) * Number(data.qty))
            var newData = {
                company_id:data.company_id,
                product_id:data.product_id,
                purchase_id:uuidv4(),
                purchase_uniq:shortid.generate(),
                sku:data.sku,
                supplier_id:data.supplier_id,
                branch_id:data.branch_id,
                ref:data.ref,
                qty:data.qty,
                notes:data.notes,
                total_amount:total_amount,
                created_by:data.created_by
            }
            var upProData = {product_id:data.product_id,
                product_qty:data.qty,
                product_purchase_price:data.purchase_price,
                product_sale_price:data.sale_price,
                product_status:(data.product_status == '1')? true:false,
                type:'purchase_product'
            }
            var sdata = {
                company_id:data.company_id,
                branch_id:data.branch_id,
                statement_uniq:shortid.generate(),
                statement_id:uuidv4(),
                particulars:'Purchase Product',
                statement_category:'purchase_product',
                ref:newData.purchase_uniq,
                statement_type:'payment',
                amount:total_amount,
                note:data.notes,
                created_by:data.created_by
    
            }
            await utilsProduct.updateProduct(upProData);
            await createPurchase(newData);
            await utilsStatement.createStatement(sdata);
            await utilsBranch.updateBranchAmount({company_id:data.company_id,branch_id:data.branch_id,amount:total_amount,type:'payment'});
            await utilsSupplier.updateSupplierAmount({company_id:data.company_id,supplier_id:data.supplier_id,amount:total_amount});
            callback({status:true,data:newData});
    
            
        }
    }catch (error){
        console.log(error)
    }
    
}

var createPurchase = (data)=>{
    return new Promise((resolve,reject)=>{
        new PurchaseProduct(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}
var getAllpurchasebycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        PurchaseProduct.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

module.exports = {purchase_management,getAllpurchasebycompany};
