var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Orders = require('../models/orders');
var moment = require('moment');
var shortid = require('shortid');

var order_management = async (data,callback)=>{
    try{
        if(data.req_type == 'addNewOrder'){
            var newData = {
                company_id:data.company_id,
                order_id:uuidv4(),
                order_uniq:shortid.generate(),
                branch_id:data.branch_id,
                products:data.products,
                shipping_by:data.shipping_by,
                customer_name:data.customer_name,
                customer_phone:data.customer_phone,
                order_title:data.order_title,
                shipping_address:data.shipping_address,
                delivery_charge:data.delivery_charge,
                vat_tax:data.vat_tax,
                payment:data.advance_payment,
                total_price:(data.total_price + data.advance_payment),
                due_price:data.total_price,
                delivery_date:data.delivery_date,
                discount_amount:data.discount_amount,
                note:data.note,
                order_reference:data.order_reference,
                total_product_price:data.total_product_price,
                total_product_purchase_price:data.total_product_purchase_price,
                order_total_cost:data.total_product_purchase_price,
                created_by:data.created_by
                
            }
    
            await utilsProduct.updateMultipleProductQty(newData);
            if(newData.payment > 0){
                var sdata = {
                    company_id:data.company_id,
                    branch_id:data.branch_id,
                    statement_uniq:shortid.generate(),
                    statement_id:uuidv4(),
                    particulars:'Advance payment from order id #'+newData.order_uniq+'',
                    statement_category:'sales_products',
                    ref:newData.order_uniq,
                    statement_type:'received',
                    amount:newData.payment,
                    note:data.note,
                    created_by:data.created_by
                }
                await utilsStatement.createStatement(sdata);
                await utilsBranch.updateBranchAmount({company_id:data.company_id,branch_id:data.branch_id,amount:sdata.amount,type:'received'});
            }
            await createNewOrder(newData); 
            var customerData = await utilsCustomer.findcustomerbyphone(data.customer_phone,data.company_id); 
            if(customerData.status){
                if(customerData.data == null){
                    var necus = {
                        company_id:data.company_id,
                        customer_id:uuidv4(),
                        customer_name:data.customer_name,
                        customer_phone:data.customer_phone,
                        customer_address:data.shipping_address,
                        customer_shipping_address:data.shipping_address,
                        created_by:data.created_by,
                        customer_status:true
                    }
                    await utilsCustomer.createNewCustomer(necus);
                }
            }

            callback({status:true,data:newData});
            
    
    
    
            
        }else if(data.req_type == 'get_single_order'){
            var getsingledata = await get_single_order(data.order_id)
            callback(getsingledata);
        }else if(data.req_type == 'updateOrder'){
            if(data.delivery_charge_amount > 0){
                var sdata = {
                    company_id:data.company_id,
                    branch_id:data.branch_id,
                    statement_uniq:shortid.generate(),
                    statement_id:uuidv4(),
                    particulars:'Delivery Charge paid for order #'+data.order_uniq+'',
                    statement_category:'pay_delivery_charge',
                    ref:data.newPaymentSource,
                    statement_type:'payment',
                    amount:data.delivery_charge_amount,
                    created_by:data.created_by
                }
                await utilsStatement.createStatement(sdata);
                await utilsBranch.updateBranchAmount({company_id:data.company_id,branch_id:data.branch_id,amount:sdata.amount,type:'payment'});
            }

            if(data.payment > 0){
                var sdata2 = {
                    company_id:data.company_id,
                    branch_id:data.branch_id,
                    statement_uniq:shortid.generate(),
                    statement_id:uuidv4(),
                    particulars:'Received due payment from order #'+data.order_uniq+'',
                    statement_category:'sales_products',
                    ref:data.newPaymentSource,
                    statement_type:'received',
                    amount:data.payment,
                    created_by:data.created_by
                }
                await utilsStatement.createStatement(sdata2);
                await utilsBranch.updateBranchAmount({company_id:data.company_id,branch_id:data.branch_id,amount:sdata2.amount,type:'received'});
            }
            if(data.order_status == 2){
                await utilsProduct.updateMultipleProductQtyreturn(data);
            }else if(data.order_status == 3){
                await utilsProduct.updateMultipleProductQtycanceled(data);
            }
            // if(data.order_status !== 2){
                await updateOrder(data);
            // }
            callback({status:true})
        }else if(data.req_type == 'order_report'){
            var getreport = await getOrderReport(data);
            callback(getreport)
        }
    }catch(error){
        console.log(77,error);
        callback({status:false,error:error})
    }
    
}


var createNewOrder = (data)=>{
    return new Promise((resolve,reject)=>{
        new Orders(data).save(function(err,result){
            if(err){
                reject({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}
var updateOrder = (data)=>{
    return new Promise((resolve,reject)=>{
        if(data.delivery_charge_amount > 0){
            data.delivery_charge_status = true;
        }
        Orders.updateOne({order_id:data.order_id},{$inc:{payment:data.payment,due_price:-data.payment,order_total_cost:data.delivery_charge_amount},order_status:data.order_status,delivery_charge_status:data.delivery_charge_status,delivery_charge_amount:data.delivery_charge_amount,order_title:data.order_title,customer_name:data.customer_name,customer_phone:data.customer_phone,shipping_address:data.shipping_address,note:data.note,shipping_by:data.shipping_by,order_reference:data.order_reference,delivery_date:data.delivery_date,return_products:data.return_products},function(err,result){
            if(err){
                reject({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        });
    })
}
var getAllOrdersListbycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.find({company_id:company_id}).sort({ created_at: -1}).exec(function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}
var get_single_order = (order_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.findOne({order_id:order_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:{}});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}
var countTotalOrderBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.countDocuments({company_id:company_id},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}
var countTotalOrderPenddingBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.countDocuments({company_id:company_id,order_status:4},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            } 
        })
    })
}
var countTotalOrderDeliveredBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.countDocuments({company_id:company_id,order_status:1},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}
var countTotalOrderReturnBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.countDocuments({company_id:company_id,order_status:2},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}
var countTotalOrderCancelBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Orders.countDocuments({company_id:company_id,order_status:3},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}
var getOrderReport = (data)=>{
    return new Promise((resolve,reject)=>{
        if(data.order_status != 'all'){
            var order_status = parseInt(data.order_status);
            Orders.find({created_at:{$gte:data.start,$lte:data.end},company_id:data.company_id,order_status:order_status},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            })
        }else{
            Orders.find({created_at:{$gte:data.start,$lte:data.end},company_id:data.company_id},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            })
        }
    })
}

module.exports = {
    order_management,createNewOrder,getAllOrdersListbycompany,countTotalOrderBycompany,countTotalOrderPenddingBycompany,
    countTotalOrderDeliveredBycompany,
    countTotalOrderReturnBycompany,
    get_single_order,
    countTotalOrderCancelBycompany
};
