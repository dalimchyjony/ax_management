var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var bcrypt = require('bcryptjs');
var _ = require('lodash');
const Login = require('../models/login');
var sfs = require('fs');
var securePin = require("secure-pin");
var verification_code = securePin.generatePinSync(6);
var moment = require('moment');
var salt = bcrypt.genSaltSync(10);

const nodemailer = require("nodemailer");

var register = async (data,callback)=>{
    if(data.req_type == 'registerNewCompany'){
        var phone = await findOnePhone(data.user_phone);
        var email = await findOneEmail(data.user_email);
        if(phone.status && email.status){
            if(phone.data != null){
                callback({status:false, msg:'Phone Number Already Exist.'});
            }else if(email.data != null ){
                callback({status:false, msg:'Email Address Already Exist.'});
            }else{
                var hash = bcrypt.hashSync(data.password, salt);
                var logindata = {
                    user_id:uuidv4(),
                    company_id:uuidv4(),
                    user_phone:data.user_phone,
                    user_email:data.user_email,
                    password:hash,
                    status:true,
                }
                var createnew = await createNewLogin(logindata);
                if(createnew.status){
                    var newCompany = {
                        company_id:logindata.company_id,
                        company_name:data.company_name,
                        company_phone:logindata.user_phone,
                        created_by:logindata.user_id
                    }
                    var createcompany = await utilsCompany.createNewCompany(newCompany);
                        if(createcompany.status){
                            var newuser = {
                                company_id:newCompany.company_id,
                                user_id:logindata.user_id,
                                user_name:data.user_name,
                                user_phone:data.user_phone,
                                user_role:'admin',
                                user_email:data.user_email,
                                created_by:logindata.user_id,
                                user_type:'partner',
                                user_designation:'ceo'
                            }
                            var createuser = await utilsUser.createNewUser(newuser);
                            if(createuser.status){
                                callback({status:true, data:newuser}); 
                            }else{
                                callback({status:false, msg:'Something Wrong!!!',err:createuser.data}); 
                            }
                        }else{
                            callback({status:false, msg:'Something Wrong!!!',err:createcompany.data}); 
                        }
                }else{
                    callback({status:false, msg:'Something Wrong!!!',err:createnew.data });
                }
            }
        }else{
            callback({status:false, msg:'Something Wrong!!!'});
        }


    }
}

var findOnePhone = (number)=>{
    return new Promise((resolve,reject)=>{
        Login.findOne({user_phone:number},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}

var findOneEmail = (email)=>{
    return new Promise((resolve,reject)=>{
        Login.findOne({user_email:email},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}

var createNewLogin = (data)=>{
    return new Promise((resolve,reject)=>{
        new Login(data).save(function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    });
}


module.exports = {register,findOneEmail,findOnePhone,createNewLogin};
