var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Files = require('../models/files');
var moment = require('moment');

var file_management = async (data,callback)=>{
    if(data.req_type == 'createNewfile'){
        var newData = {
            company_id:data.company_id,
            file_id:uuidv4(),
            file_name:data.file_name,
            file_type:data.file_type,
            created_by:data.created_by
        }
        var create_file = await createNewFile(newData);
        if(create_file.status){
            callback({status:true, data:newData}); 
        }else{
            callback({status:false, msg:'Something Wrong!!!',err:create_category.data});  
        }
    }
}

var createNewFile=(data)=>{
    return new Promise((resolve,reject)=>{
        new Files(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var getAllFilebycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Files.find({company_id:company_id}).sort({created_at: -1}).exec(function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

module.exports = {getAllFilebycompany,file_management};
