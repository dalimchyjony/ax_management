var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Branch = require('../models/branch');
var moment = require('moment');

var branch_management = async (data,callback)=>{
    if(data.req_type == 'createNewBranch'){
        var newData = {
            company_id:data.company_id,
            branch_id:uuidv4(),
            branch_name:data.branch_name,
            branch_address:data.branch_address,
            branch_email:data.branch_email,
            branch_phone:data.branch_phone,
            created_by:data.created_by,
            branch_status:(data.branch_status == '1')? true:false
        }
        var findBranch = await findbranchbyName(newData.branch_name,newData.company_id);
        if(findBranch.status){
            if(findBranch.data == null){
                var newbranch = await createNewBranch(newData);
                if(newbranch.status){
                    callback({status:true,data:newbranch.data});
                }else{
                    callback({status:false,msg:err});
                }
            }else{
                callback({status:false,msg:'Branch Name Already Exists'});
            }
        }else{
            callback({status:false,msg:err});
        }

        
    }
}

var createNewBranch=(data)=>{
    return new Promise((resolve,reject)=>{
        new Branch(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var findbranchbyName=(branch_name,company_id)=>{
    return new Promise((resolve,reject)=>{
        Branch.findOne({branch_name:branch_name,company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}
var updateBranchAmount =(data)=>{
    return new Promise((resolve,reject)=>{
        if(data.type == 'payment'){
            Branch.updateOne({company_id:data.company_id,branch_id:data.branch_id},{$inc: { current_balance: -data.amount }},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            });
        }else{
            Branch.updateOne({company_id:data.company_id,branch_id:data.branch_id},{$inc: { current_balance: data.amount }},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            });
        }
        
    })
}

var getAllbranchbycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Branch.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

module.exports = {branch_management,getAllbranchbycompany,updateBranchAmount};
