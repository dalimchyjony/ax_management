var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Company = require('../models/company');
var moment = require('moment');

var createNewCompany=(data)=>{
    return new Promise((resolve,reject)=>{
        new Company(data).save(function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}
var findCompanyInfo=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Company.findOne({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}


module.exports = {createNewCompany,findCompanyInfo};
