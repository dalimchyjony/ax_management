var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Customer = require('../models/customer');
var moment = require('moment');

var customer_management = async (data,callback)=>{
    if(data.req_type == 'createNewCustomer'){
        var newData = {
            company_id:data.company_id,
            customer_id:uuidv4(),
            customer_name:data.customer_name,
            customer_phone:data.customer_phone,
            customer_email:data.customer_email,
            customer_address:data.customer_address,
            customer_shipping_address:data.customer_shipping_address,
            created_by:data.created_by,
            customer_status:(data.customer_status == '1')? true:false
        }
        var phone = await findcustomerbyphone(newData.customer_phone,newData.company_id);
        var email = await findcustomerbyemail(newData.customer_email,newData.company_id);

        if(phone.status && email.status){
            if(phone.data != null){
                callback({status:false, msg:'Phone Number Already Exist.'});
            }else if(email.data != null ){
                callback({status:false, msg:'Email Address Already Exist.'});
            }else{
                var createCustomer = await createNewCustomer(newData);
                if(createCustomer.status){
                    callback({status:true, data:newData}); 
                }else{
                    console.log(34,createCustomer);
                    callback({status:false, msg:'Something Wrong!!!',err:createCustomer.data}); 
                }
            }
        }else{
            console.log(39,phone,email)
            callback({status:false, msg:'Something Wrong!!!'});
        }
    }
}
var findcustomerbyphone = (customer_phone,company_id)=>{
    return new Promise((resolve,reject)=>{
        Customer.findOne({customer_phone:customer_phone,company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}

var findcustomerbyemail = (customer_email,company_id)=>{
    return new Promise((resolve,reject)=>{
        Customer.findOne({customer_email:customer_email,company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,data:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    });
}


var createNewCustomer=(data)=>{
    return new Promise((resolve,reject)=>{
        new Customer(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var getAllcustomerbycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Customer.find({company_id:company_id},function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}
var countTotalCustomerBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Customer.countDocuments({company_id:company_id},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}

module.exports = {customer_management,getAllcustomerbycompany,createNewCustomer,findcustomerbyphone,countTotalCustomerBycompany};
