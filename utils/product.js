var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Product = require('../models/product');
var moment = require('moment');
const db = require('../config/keys').mongoURI;


var product_management = async (data,callback)=>{
    if(data.req_type == 'createNewProduct'){
        var newData = {
            company_id:data.company_id,
            product_id:uuidv4(),
            sku:data.sku,
            category_id:data.category_id,
            product_for:data.product_for,
            product_name:data.product_name,
            product_price:data.product_price,
            product_sale_price:data.product_sale_price,
            product_slug:data.product_slug,
            product_image:data.product_image,
            feature_image:data.feature_image,
            product_desc:data.product_desc,
            product_short_desc:data.product_short_desc,
            created_by:data.created_by,
            product_status:(data.product_status == '1')? true:false
        }
        var findSKU = await findProductSku(newData.sku,newData.company_id);
        if(findSKU.data.length > 0){
            callback({status:false, msg:'SKU already exist.'}); 
        }else{
            var find_slug = await findproduct_by_slug(newData.product_slug);
            if(find_slug.status){
                if(find_slug.data.length > 0){
                    newData.product_slug = newData.product_slug+'-'+(find_slug.data.length + 1); 
                }
                var createPoduct = await createNewProduct(newData);
                callback(createPoduct);
            }else{
                callback({status:false, msg:'Something Wrong!!!'});
            }
        }

        
    }else if(data.req_type == 'get_single_product'){
        var findProduct = await findProductByid(data.product_id);
        callback(findProduct);
    }else if(data.req_type == 'update_single_product'){
        var updateProduct = await findupdateProductByid(data);
        callback(updateProduct);
    }
}

var findProductSku = (sku,company_id)=>{
    return new Promise((resolve,reject)=>{
        Product.find({sku:sku,company_id:company_id},function(err,result){
            if(err){
                reject({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var findProductByid = (product_id)=>{
    return new Promise((resolve,reject)=>{
        Product.findOne({product_id:product_id},function(err,result){
            if(err){
                reject({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var findupdateProductByid = (data)=>{
    return new Promise((resolve,reject)=>{
        Product.updateOne({product_id:data.product_id},{product_name:data.product_name,feature_image:data.feature_image,product_image:data.product_image,product_desc:data.product_desc,product_short_desc:data.product_short_desc},function(err,result){
            if(err){
                reject({status:false,err:err});
            }else{
                resolve({status:true,data:result});
            }
        });
    })
}

var findproduct_by_slug = (product_slug)=>{
    return new Promise((resolve,reject)=>{
        Product.find({product_slug:product_slug},function(err,result){
            if(err){
                reject({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}


var createNewProduct=(data)=>{
    return new Promise((resolve,reject)=>{
        new Product(data).save(function(err,result){
            if(err){
                reject({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var getAllProductbycompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Product.find({company_id:company_id}).sort({created_at: -1}).exec(function(err,result){
            if(err){ 
                reject({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var updateProduct=(data)=>{
    return new Promise((resolve,reject)=>{
        if(data.type == 'purchase_product'){
            Product.updateOne({product_id:data.product_id},{$inc: { product_qty: data.product_qty,total_purchase:data.product_qty },product_purchase_price:data.product_purchase_price,product_sale_price:data.product_sale_price,product_status:data.product_status},function(err,result){
                if(err){
                    reject({status:false,data:err});
                }else{
                    resolve({status:true,data:result});
                }
            })
        }
    })
}
var updateMultipleProductQty=(data)=>{
    return new Promise((resolve,reject)=>{

        var bulk = Product.collection.initializeOrderedBulkOp();

        _.each(data.products,function(v,k){
            bulk.find( { company_id: data.company_id,product_id:v.product_id } ).upsert().updateOne( { $inc: { product_qty: - v.qty,total_sale:v.qty }  } );
        });

        bulk.execute(function (error,result) {
           if(error){
                reject({status:false,err:error});
           }else{
                resolve({status:true,data:result});
           }             
       });
    })
}
var updateMultipleProductQtyreturn=(data)=>{
    return new Promise((resolve,reject)=>{

        var bulk = Product.collection.initializeOrderedBulkOp();

        _.each(data.return_products,function(v,k){
            bulk.find( { company_id: data.company_id,product_id:v.product_id } ).upsert().updateOne( { $inc: { product_qty: v.qty,total_return:v.qty }  } );
        });

        bulk.execute(function (error,result) {
           if(error){
                reject({status:false,err:error});
           }else{
                resolve({status:true,data:result});
           }             
       });
    })
}
var updateMultipleProductQtycanceled=(data)=>{
    return new Promise((resolve,reject)=>{

        var bulk = Product.collection.initializeOrderedBulkOp();

        _.each(data.return_products,function(v,k){
            bulk.find( { company_id: data.company_id,product_id:v.product_id } ).upsert().updateOne( { $inc: { product_qty: v.qty,total_canceled:v.qty }  } );
        });

        bulk.execute(function (error,result) {
           if(error){
                reject({status:false,err:error});
           }else{
                resolve({status:true,data:result});
           }             
       });
    })
}

var countTotalProductBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Product.countDocuments({company_id:company_id},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}
var countTotalLowStocksBycompany = (company_id)=>{
    return new Promise((resolve,reject)=>{
        Product.countDocuments({company_id:company_id,product_qty:{$gte:5}},function(err,count){
            if(err){
                resolve(0);
            }else{
                resolve(count);
            }
        })
    })
}

module.exports = {product_management,getAllProductbycompany,updateProduct,updateMultipleProductQty,countTotalProductBycompany,countTotalLowStocksBycompany,updateMultipleProductQtyreturn,updateMultipleProductQtycanceled};
