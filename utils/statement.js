var express = require('express');
var router = express.Router();
const uuidv4 = require('uuid/v4');
var _ = require('lodash');
const Statement = require('../models/statement');
var moment = require('moment');
var shortid = require('shortid');

var statement_management = async (data,callback)=>{
    try{
        if(data.req_type == 'createNewStatement' || data.req_type == 'withdrawORinvest' || data.req_type == 'salary'){
            var sdata = {
                company_id:data.company_id,
                branch_id:data.branch_id,
                statement_uniq:shortid.generate(),
                statement_id:uuidv4(),
                particulars:data.particulars,
                statement_category:data.statement_category,
                ref:data.ref,
                statement_type:data.statement_type,
                amount:data.amount,
                note:data.note,
                created_by:data.created_by
            }
            await createStatement(sdata);
            await utilsBranch.updateBranchAmount({company_id:data.company_id,branch_id:data.branch_id,amount:data.amount,type:data.statement_type});
            if(data.req_type == 'withdrawORinvest' ||  data.req_type == 'salary'){
                await utilsUser.updateSingeleUser(data);
            }
            callback({status:true,data:sdata});
        }else if(data.req_type == 'report_with_date'){
            var getreport = await getStatementReportbydate(data);
            callback(getreport)
        }
    }catch(error){
        console.log(error)
    }
}

var createStatement=(data)=>{
    return new Promise((resolve,reject)=>{
        new Statement(data).save(function(err,result){
            if(err){
                resolve({status:false,err:err});
            }else{
                resolve({status:true,data:data});
            }
        });
    })
}

var getAllStatementByCompany=(company_id)=>{
    return new Promise((resolve,reject)=>{
        Statement.find({company_id:company_id}).sort({created_at: -1}).exec(function(err,result){
            if(err){
                resolve({status:false,err:err,data:[]});
            }else{
                resolve({status:true,data:result});
            }
        })
    })
}

var getStatementReportbydate = (data)=>{
    return new Promise((resolve,reject)=>{
        if(data.statement_type != 'all'){
            Statement.find({created_at:{$gte:data.start,$lte:data.end},company_id:data.company_id,statement_type:data.statement_type},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            })
        }else{
            Statement.find({created_at:{$gte:data.start,$lte:data.end},company_id:data.company_id},function(err,result){
                if(err){
                    resolve({status:false,err:err});
                }else{
                    resolve({status:true,data:result});
                }
            })
        }
    })
}

module.exports = {createStatement,getAllStatementByCompany,statement_management};
