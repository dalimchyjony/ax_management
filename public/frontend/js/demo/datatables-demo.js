// Call the dataTables jQuery plugin
$(document).ready(function() {
   $('#dataTable').DataTable();
   $('#statementDatatable').DataTable( {
      "order": [[ 0, "asc" ]]
  } );
   $('#ordersDatatable').DataTable( {
      "order": [[ 0, "asc" ]]
  } );
  $('#item_list_dataTable').DataTable( {
      "order": [[ 0, "asc" ]]
  } );
});
