setInterval(function(){ 
    forcallimgfunwork();

 }, 1000);
 
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
// Custom Javascripts

function removeA(arr) {
	var what, a = arguments, L = a.length, ax;
	while (L > 1 && arr.length) {
		what = a[--L];
		while ((ax = arr.indexOf(what)) !== -1) {
			arr.splice(ax, 1);
		}
	}
	return arr;
}

var imggallaryArray = [];
var itemCategory = [];
var product_imgs = [];
var feature_img = null;
var socketImages = [];
var actionItem = null;
function createNewBrance(){
    var data = {
        branch_name:$('#branch_name').val(),
        branch_address:$('#branch_address').val(),
        branch_email:$('#branch_email').val(),
        branch_phone:$('#branch_phone').val(),
        branch_status:$('#branch_status').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewBranch'
    }
    if(data.branch_name.length < 2){
        openWarning('name');
    }else if(data.branch_phone.length < 10){
        openWarning('phone');
    }else if(!validateEmail(data.branch_email)){
        openWarning('email');
    }else{
        socket.emit('branch_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Branch Added.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });
    }

}
function createSupplier(){
    var data = {
        supplier_name:$('#supplier_name').val(),
        supplier_phone:$('#supplier_phone').val(),
        supplier_email:$('#supplier_email').val(),
        supplier_address:$('#supplier_address').val(),
        supplier_status:$('#supplier_status').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewSupplier'
    }
    if(data.supplier_name.length < 2){
        openWarning('name');
    }else if(data.supplier_phone.length < 10){
        openWarning('phone');
    }else{
        socket.emit('supplier_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Supplier Added.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });
    }

}


function createNewUser(){
    var salary = $('#salary').val()
        if(salary == ''){
            salary = 0;
        }else{
            salary = parseInt(salary) 
        }
    var data = {
        user_name:$('#user_name').val(),
        user_phone:$('#user_phone').val(),
        user_role:$('#user_role').val(),
        user_email:$('#user_email').val(),
        user_address:$('#user_address').val(),
        user_gender:$('#user_gender').val(),
        user_nid:$('#user_nid').val(),
        user_dob:$('#user_dob').val(),
        user_type:$('#user_type').val(),
        user_designation:$('#user_designation').val(),
        salary:salary,
        status:$('#status').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewUser'
    }
    if(data.user_name.length < 2){
        openWarning('name');
    }else if(data.user_phone.length < 10){
        openWarning('phone');
    }else if(!validateEmail(data.user_email)){
        openWarning('email');
    }else{
        socket.emit('user_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully User Added.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });
    }
}

function createNewCustomer(){
    var data = {
        customer_name:$('#customer_name').val(),
        customer_phone:$('#customer_phone').val(),
        customer_email:$('#customer_email').val(),
        customer_address:$('#customer_address').val(),
        customer_shipping_address:$('#customer_shipping_address').val(),
        customer_status:$('#customer_status').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewCustomer'
    }
    if(data.customer_name.length < 2){
        openWarning('name');
    }else if(data.customer_phone.length < 10){
        openWarning('phone');
    }else if(!validateEmail(data.customer_email)){
        openWarning('email');
    }else{
        socket.emit('customer_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Customer Added.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });
    }
}

function createCategory(){
    var data = {
        category_name:$('#category_name').val(),
        parent_category_id:$('#parent_category_id').val(),
        category_status:$('#category_status').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewCategory'
    }
    if($('#parent_category_id').val() == 'null'){
        data.parent_category_id = null;
    }
    if(data.category_name.length < 2){
        openWarning('category_name');
    }else{
        var category_slug = $('#category_name').val().split(' ').join('-');
        data['category_slug'] = category_slug;
        socket.emit('category_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Category Added.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });
    }
}

function createNewProduct(){
    var data = {
        product_name:$('#product_name').val(),
        sku:$('#itemSku').val().split(" ").join(''),
        category_id:itemCategory,
        feature_image:feature_img,
        product_image:product_imgs,
        product_for:$('#product_for').val(),
        product_price:$('#product_price').val(),
        product_sale_price:$('#product_sale_price').val(),
        product_status:$('#product_status').val(),
        product_short_desc:$('#product_short_desc').val(),
        product_desc:$('#product_desc').val(),
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewProduct'
    }

    if(data.product_name.length < 2){
        openWarning('product_name');
    }else if(data.sku.length < 2){
        openWarning('sku');
    }else if(data.feature_image == null || data.feature_image == ''){
         openWarning('feature_image');
    }else{
        var product_slug = $('#product_name').val().split(' ').join('-');
        data['product_slug'] = product_slug;
        socket.emit('product_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Item Saved.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        });

    }
}

function categoryPush(elm){
    var category_id = $(elm).val();
    if(itemCategory.indexOf(category_id) == -1){
        itemCategory.push(category_id);
    }
}

function selectGallaryImg(elm){
    var file_id = $(elm).attr('data-id');
    if(imggallaryArray.indexOf(file_id) == -1){
        imggallaryArray.push(file_id);
        $(elm).addClass('active');
    }else{
        removeA(imggallaryArray,file_id);
        $(elm).removeClass('active');
    }
    if(imggallaryArray.length > 0){
        $('#selectFiles').show();
    }else{
        $('#selectFiles').hide(); 
    }
}

function selectForAction(){
   var page =  $('#thispagetitle').val();
   if(page == 'additem' || $('#editItemDiv').is(':visible')){
    $.each(socketImages,function(k,v){
        if(imggallaryArray.indexOf(v.file_id) != -1){
            if($('._item_img_'+v.file_id).length == 0){
                $('#itemImageGallary').prepend(returngallaryDesign(v,'itemGallary'))
            }
        }
    });
    if($('#itemImageGallary').find('.feature_img').length == 0){
        $('#itemImageGallary').find('.col-md-3').first().click();
    }
   } 

   closeBackwrap();
}

function featureImgSelect(elm){
    $('#itemImageGallary').find('.feature_img').removeClass('feature_img');
    $(elm).addClass('feature_img');
    feature_img = $(elm).attr('data-id');

}


function viewIdtoImg(){
    $.each(socketImages,function(k,v){
        // console.log(394,'/uploads/'+v.company_id+'/'+v.file_name+'')
        $('._imgTag_'+v.file_id).attr('src','/uploads/'+v.company_id+'/'+v.file_name+'');
        $('._imgHref_'+v.file_id).attr('href','/uploads/'+v.company_id+'/'+v.file_name+'');
    })
}

$(function(){
    socket.emit('getAllFilebycompany',$('#usercompanyid').val(),function(res){
        if(res.status){
            socketImages = res.data;
            viewIdtoImg();
        }
    });
    $('#order_date').val(moment().format('mm/dd/yyyy')); 
    $('#delivery_date').val(moment().format('mm/dd/yyyy'));
});


function showAllProduct(){
    $('body').addClass('modal-open');
    $('body').append('<div class="modal-backdrop fade show" id="backwrapcolor"></div>');
    $('#productListModal').show();
    $('#productListRow').html('');
    socket.emit('getAllProductbycompany',$('#usercompanyid').val(),function(res){
        if(res.status){
            $.each(res.data,function(k,v){
                $('#productListRow').prepend(returngallaryDesign(v,'product_purchase'))
            });
            viewIdtoImg();
        }
    })
}
var selectedPurchaseProduct_id = null;
function selectPurchaseProduct(product_id,sku,feature_image,product_name,qty,product_sale_price,product_purchase_price){
    closeBackwrap();
    var page = $('#thispagetitle').val();
    if(page == 'purchase_item'){
        var design = '<div class="col-md-6 float-left"><div class="form-group"><label for="selectedItemName">Product Name</label><input disabled type="text" class="form-control" id="selectedItemName" value="'+product_name+'"></div></div>'
                    +'<div class="col-md-6 float-left"><div class="form-group"><label for="selectedItemSku">Product SKU</label><input disabled type="text" class="form-control" id="selectedItemSku" value="'+sku+'"></div></div>'
        
        selectedPurchaseProduct_id = product_id;
    
        $('#selectedProduct').html('');
        $('#selectedProduct').html(design);
    }else if(page == 'add_order'){

        var design = '<div id="_order_product_this_'+product_id+'" class="_order_items" data-id="'+product_id+'" data-price="'+product_sale_price+'">'
                        +'<div class="col-md-3 float-left"><div class="form-group"><label for="_orderPName_'+product_id+'">Product Name</label><input disabled type="text" class="form-control" id="_orderPName_'+product_id+'" value="'+product_name+'"></div></div>'
                        +'<div class="col-md-3 float-left"><div class="form-group"><label for="_orderPSKU_'+product_id+'">Product SKU</label><input disabled type="text" class="form-control" id="_orderPSKU_'+product_id+'" value="'+sku+'"></div></div>'
                        +'<div class="col-md-2 float-left"><div class="form-group"><label for="_orderP_'+product_id+'">Product Qty</label><input type="number" onblur="blurInputNumber(this,\'orderqty\')" class="form-control" onchange="thisproductchangeQty(this)" id="_orderP_'+product_id+'" value="1" max="'+qty+'" data-price="'+product_sale_price+'" data-id="'+product_id+'"></div></div>'
                        +'<div class="col-md-2 float-left"><div class="form-group"><label for="_orderP_price_'+product_id+'">Product Price</label><input disabled type="number" class="form-control product_price_box" id="_orderP_price_'+product_id+'" value="'+product_sale_price+'" data-price="'+product_sale_price+'" data-purchase="'+product_purchase_price+'" min="0"></div></div>'
                        +'<div class="col-md-2 float-left text-center"><div class="form-group"><label>Remove</label><button type="button" onclick="removeThisProductOrder(\''+product_id+'\')" class="btn-block btn btn-danger"><i class="fas fa-trash"></i></button></div>'
                    +'</div>';

        if($('#_orderP_'+product_id).length == 0){
            $('#selectedProduct').append(design);
        }else{
            $('#_orderP_'+product_id).val(1);
            $('#_orderP_price_'+product_id).val(product_sale_price);
        }
        changeTotalPrice();
    }else if(page == 'orders'){
        var design = '<div id="_order_product_this_'+product_id+'" class="_order_items" data-id="'+product_id+'" data-price="'+product_sale_price+'">'
                        +'<div class="col-md-3 float-left"><div class="form-group"><label for="_orderPName_'+product_id+'">Product Name</label><input disabled type="text" class="form-control" id="_orderPName_'+product_id+'" value="'+product_name+'"></div></div>'
                        +'<div class="col-md-3 float-left"><div class="form-group"><label for="_orderPSKU_'+product_id+'">Product SKU</label><input disabled type="text" class="form-control" id="_orderPSKU_'+product_id+'" value="'+sku+'"></div></div>'
                        +'<div class="col-md-2 float-left"><div class="form-group"><label for="_orderP_'+product_id+'">Product Qty</label><input type="number" onblur="blurInputNumber(this,\'orderqty\')" class="form-control" onchange="thisproductchangeQty(this)" id="_orderP_'+product_id+'" value="1" max="'+qty+'" data-price="'+product_sale_price+'" data-id="'+product_id+'"></div></div>'
                        +'<div class="col-md-2 float-left"><div class="form-group"><label for="_orderP_price_'+product_id+'">Product Price</label><input disabled type="number" class="form-control product_price_box" id="_orderP_price_'+product_id+'" value="'+product_sale_price+'" data-price="'+product_sale_price+'" data-purchase="'+product_purchase_price+'" min="0"></div></div>'
                        +'<div class="col-md-2 float-left text-center"><div class="form-group"><label>Remove</label><button type="button" onclick="removeThisProductOrder(\''+product_id+'\')" class="btn-block btn btn-danger"><i class="fas fa-trash"></i></button></div>'
                    +'</div>';
        return design;
    }
    
}

function removeThisProductOrder(product_id){
    if(page == 'orders'){
        $('#_orderP_'+product_id).val($('#_orderP_'+product_id).attr('max'));
        $('#_order_product_this_'+product_id).hide();
    }else{

        $('#_order_product_this_'+product_id).remove();
        changeTotalPrice();
    }
}

function thisproductchangeQty(elm){
    var product_id = $(elm).attr('data-id');
    var product_price = parseInt($(elm).attr('data-price'));
    var productQty = parseInt($(elm).val());
    var totalPrice = (product_price * productQty);
    $('#_orderP_price_'+product_id).val(totalPrice);
    changeTotalPrice();

}

function changeTotalPrice(){
    var productPrice = 0;
    $.each($('.product_price_box'),function(k,v){
        productPrice = (parseInt($(v).val()) + productPrice);
    });
    if(productPrice > 0){
        var otherAmount = 0
        otherAmount = (parseInt($('#vat_tax').val()) + otherAmount);
        otherAmount = (parseInt($('#delivery_charge').val()) + otherAmount);
        productPrice = (productPrice + otherAmount);
        productPrice = (productPrice - (parseInt($('#discount_amount').val()) + parseInt($('#advance_payment').val())));
    }
    $('#product_total_price').val(productPrice);
}

function returngallaryDesign(data,type){
    var page = $('#thispagetitle').val();
    if(type == 'product_purchase'){
         if(page == 'add_order'){
             if(data.product_qty != 0){
                var html = '<div class="col-md-3 border border-primary rounded-lg m-3 perimg lazy" onclick="selectPurchaseProduct(\''+data.product_id+'\',\''+data.sku+'\',\''+data.feature_image+'\',\''+data.product_name+'\','+data.product_qty+','+data.product_sale_price+','+data.product_purchase_price+')" style="cursor:pointer">'
                                +'<img class="img-fluid _imgTag_'+data.feature_image+'" src="" alt="Feature Image">'
                                +'<span class="skutitle">'+data.sku+'</span>'
                            +'</div>'
                return html;
             }
         }else{
             var html = '<div class="col-md-3 border border-primary rounded-lg m-3 perimg lazy" onclick="selectPurchaseProduct(\''+data.product_id+'\',\''+data.sku+'\',\''+data.feature_image+'\',\''+data.product_name+'\','+data.product_qty+','+data.product_sale_price+','+data.product_purchase_price+')" style="cursor:pointer">'
                             +'<img class="img-fluid _imgTag_'+data.feature_image+'" src="" alt="Feature Image">'
                             +'<span class="skutitle">'+data.sku+'</span>'
                         +'</div>'
             return html;
         }

    }else{
        if(data.file_type == 'img'){
            if(type == 'itemGallary'){
                var html = '<div class="col-md-3 border border-primary rounded-lg m-3 float-left perimg lazy _item_img_'+data.file_id+'" onclick="featureImgSelect(this)" data-id="'+data.file_id+'">'
                            +'<img class="img-fluid" src="/uploads/'+data.company_id+'/'+data.file_name+'" alt="">'
                        +'</div>'
            }else{
                var html = '<div class="col-md-3 border border-primary rounded-lg m-3 perimg lazy" >'
                            +'<div class="custom_checkbox" onclick="selectGallaryImg(this)" data-id="'+data.file_id+'"></div>'
                            +'<img class="img-fluid" src="/uploads/'+data.company_id+'/'+data.file_name+'" alt="">'
                        +'</div>'
            }
            
            return html;
        }else{
            return '';
        }
    }
}

function addPurchaseItem(){
    var purchase_price = $('#product_purchase_price').val();
    var sale_price = $('#product_sale_price').val();
    var data = {
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'addNewPurchase',

        product_id:selectedPurchaseProduct_id,
        supplier_id:$('#supplier_id').val(),
        branch_id:$('#branch_id').val(),
        sku:$('#selectedItemSku').val(),
        ref:$('#voucher_no').val(),
        qty:$('#quantity').val(),
        notes:$('#notes').val(),
        product_status:$('#product_status').val(),
        purchase_price:purchase_price,
        sale_price:sale_price
    }
   
    if(purchase_price < 1){
        openWarning('purchase_price');
    }else if(sale_price < 1 ){
        openWarning('sale_price');
    }else if(selectedPurchaseProduct_id == null){
        openWarning('select_product');
    }else if(data.qty  < 1){
        openWarning('qty');
    }else if(data.supplier_id == 'null'){
        openWarning('supplier');
    }else if(data.branch_id == 'null'){
        openWarning('branch');
    }else if(data.ref.length == 0){
        openWarning('ref');
    }else{
        socket.emit('purchase_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Purchase Item.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.msg,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        })
    }
}
var createOrderBtn = true;
function createOrder(){
    if(createOrderBtn){
        createOrderBtn = false;
        var products = []
        var data = {
            company_id:$('#usercompanyid').val(),
            created_by:$('#userid').val(),
            req_type:'addNewOrder',
            branch_id:$('#branch_id').val(),
            order_title:$('#order_title').val(),
            customer_phone:$('#customer_Phone').val(),
            customer_name:$('#customer_name').val(),
            shipping_by:$('#shipping_by').val(),
            shipping_address:$('#shipping_address').val(),
            delivery_charge:parseInt($('#delivery_charge').val()),
            vat_tax:parseInt($('#vat_tax').val()),
            discount_amount:parseInt($('#discount_amount').val()),
            advance_payment:parseInt($('#advance_payment').val()),
            total_price:parseInt($('#product_total_price').val()),
            delivery_date:$('#delivery_date').val(),
            order_reference:$('#order_reference').val(),
            note:$('#orderNotes').val()
        }
        var total_product_price = 0;
        var total_product_purchase_price = 0;
    
        $.each($('._order_items'),function(k,v){
            var product_id = $(v).attr('data-id');
            var data = {
                product_id:product_id,
                sku:$('#_orderPSKU_'+product_id).val(),
                qty:parseInt($('#_orderP_'+product_id).val()),
                purchase_price:parseInt($('#_orderP_price_'+product_id).attr('data-purchase')),
                price:parseInt($('#_orderP_price_'+product_id).val()),
            }
            total_product_price = total_product_price + data.price;
            total_product_purchase_price = (total_product_purchase_price + data.purchase_price) * data.qty;
            products.push(data);
        });
    
        if(data.order_title == ''){
            openWarning('order_title');
        }else if(products.length == 0){
            openWarning('add_product');
        }else if(data.branch_id.length < 3){
            openWarning('branch');
        }else if(data.shipping_by.length < 2){
            openWarning('shipping_by');
        }else if(data.shipping_address.length < 3){
            openWarning('shipping_address');
        }else if(data.delivery_charge < 0){
            openWarning('delivery_charge');
        }else if(data.vat_tax < 0){
            openWarning('vat_tax');
        }else if(data.total_price < 0){
            openWarning('total_price');
        }else if(data.customer_phone < 10){
            openWarning('customer_Phone');
        }else if(data.customer_name < 3){
            openWarning('customer_name');
        }else if(!data.delivery_date){
            openWarning('delivery_date');
        }else{
    
            data['total_product_price'] = total_product_price;
            data['total_product_purchase_price'] = total_product_purchase_price;
            data['products'] = products;
    
            socket.emit('order_management',data,function(res){
                createOrderBtn = true;
                if(res.status){
                    Swal.fire({
                        title: 'Success!!!',
                        text: "Successfully Added Order.",
                        type: 'success',
                        confirmButtonText: 'ok'
                    }).then((result) => {
                        location.reload();
                    })
                }else{
                    Swal.fire({
                        title: 'Error!!!',
                        text: res.msg,
                        type: 'error',
                        confirmButtonText: 'ok'
                    }).then((result) => {
                        // location.reload();
                    });
                }
            })
        }
    }else{
        Swal.fire({
            title: 'Error!!!',
            text: 'Please waiting for response.',
            type: 'error',
            confirmButtonText: 'ok'
        }).then((result) => {
            // location.reload();
        });
    }


}

function openWarning(type){
    createOrderBtn = true;
    var msg = switchreturnmsg(type);

    Swal.fire('Warning!!!',msg,'warning');
}


function switchreturnmsg(type){

    switch (type) {
        case 'email':
            return "Email Address is invalid. Please provide valid email address.";
        case 'phone':
            return "Invalid Phone Number.";
        case 'particulars':
            return "Particulars minimum 5 length required.";
        case 'name':
            return "Name minimum 3 length required.";
        case 'category_name':
            return "Category Name minimum 3 length required.";
        case 'product_name':
            return "Product Name minimum 3 length required.";
        case 'sku':
            return "SKU is required.";
        case 'supplier':
            return "Supplier is required.";
        case 'purchase_price':
            return "Purchase Price is required.";
        case 'sale_price':
            return "Sale Price is required.";
        case 'select_product':
            return "Please Select Product.";
        case 'qty':
            return "Qty minimun length 1 required.";
        case 'branch':
            return "Branch is required.";
        case 'ref':
            return "Ref. is required.";
        case 'add_product':
            return "Select minimum one product.";
        case 'shipping_by':
            return "Select shipping by.";
        case 'shipping_address':
            return "Shipping Address required.";
        case 'delivery_charge':
            return "Enter valid value for delivery charge.";
        case 'vat_tax':
            return "Enter valid value for vat tax.";
        case 'advance_payment':
            return "Enter valid value for advance payment.";
        case 'total_price':
            return "Enter valid value for total price.";
        case 'delivery_date':
            return "Delivery date required.";
        case 'customer_name':
            return "Customer Name required.";
        case 'customer_Phone':
            return "Invalid Customer Phone Number.";
        case 'amount':
            return "Invalid Amount";
        case 'statement_category':
            return "Select Statement Category.";
        case 'user_id':
            return "Please select any user.";
        case 'order_title':
            return "Order Title Required.";
        case 'feature_image':
            return "Feature Image Required.";
        default:
            return "Something Wrong!!!";
      }
}

function forcallimgfunwork(){
    var allimgSrc = $('img');
    var callImgfun = true;
    if(allimgSrc.length > 0){
        $.each(allimgSrc,function(k,v){
            if($(v).attr('src').length != undefined){
                if($(v).attr('src').length == 0){
                    if(callImgfun){
                        callImgfun = false;
                        viewIdtoImg();
                    }
                }
            }
        })
    }
}

var actionOrderid = null;

function orderActions(order_id){
    actionOrderid = null;
    socket.emit('order_management',{order_id:order_id,req_type:'get_single_order'},function(res){
       if(res.status){
           console.log(742,res.data)
           actionOrderid = res.data.order_id;
           $('#fullOrderListView').hide();
           $('#orderActionsPage').show();
           $('#order_status').val(res.data.order_status);
           $('#total_price').val(res.data.total_price);
           $('#total_payment').val(res.data.payment);
           $('#order_discount_amount').val(res.data.discount_amount);
           $('#order_delivery_charge').val(res.data.delivery_charge);
           $('#order_vat_tax').val(res.data.vat_tax);
           $('#due_amount').val(res.data.due_price);
           $('#total_product_price').val(res.data.total_product_price);
           $('#order_title').val(res.data.order_title); 
           $('#customer_name').val(res.data.customer_name); 
           $('#customer_Phone').val(res.data.customer_phone); 
           $('#shipping_address').val(res.data.shipping_address); 
           $('#orderNotes').val(res.data.note); 
           $('#shipping_by').val(res.data.shipping_by); 
           $('#order_reference').val(res.data.order_reference); 
           $('#delivery_date').val(res.data.delivery_date); 
           $('#returnProductData').hide();
           $('#returnProductData').html('');
           $('#returnProductData').html('<h3 class="text-center col-md-12">Return Product List</h3>');
           $.each(res.data.products,function(k,v){
                var design = '<div id="_order_product_this_'+v.product_id+'" class="_order_items col-md-12" data-id="'+v.product_id+'" data-price="'+v.price+'">'
                                +'<div class="col-md-4 float-left"><div class="form-group"><label for="_orderPSKU_'+v.product_id+'">Product SKU</label><input disabled type="text" class="form-control" id="_orderPSKU_'+v.product_id+'" value="'+v.sku+'"></div></div>'
                                +'<div class="col-md-4 float-left"><div class="form-group"><label for="_orderP_'+v.product_id+'">Product Qty</label><input type="number" onblur="blurInputNumber(this,\'orderqty\')" class="form-control"  id="_orderP_'+v.product_id+'" value="'+v.qty+'" min="1" max="'+v.qty+'" data-price="'+v.price+'" data-id="'+v.product_id+'"></div></div>'
                                +'<div class="col-md-4 float-left text-center"><div class="form-group"><label>Remove</label><button type="button" onclick="removeThisProductOrder(\''+v.product_id+'\')" class="btn-block btn btn-danger"><i class="fas fa-trash"></i></button></div>'
                            +'</div>';
                $('#returnProductData').append(design);
           })

           $('#newPayment').focus();
           if(res.data.delivery_charge_status == false){
            $('#delivery_charge_amount_status').show();
           }else{
            $('#delivery_charge_amount_status').hide();
           }
           if(res.data.due_price == 0){
               $('#newPaymentAmount').hide();
           }else{
                $('#newPaymentAmount').show();
           }
       }else{
        Swal.fire({
            title: 'Error!!!',
            text: res.err,
            type: 'error',
            confirmButtonText: 'ok'
        }).then((result) => {
            // location.reload();
        });
       }
    })
}

function changeOrderStatus(elm){
    var value = $(elm).val();
    if(value == '2'){
        $('#returnProductData').show();
    }else{
        $('#returnProductData').hide();
    }
}


function orderActionSavefun(){
    socket.emit('order_management',{order_id:actionOrderid,req_type:'get_single_order'},function(res){
        if(res.status){
            var delivery_charge_amount = $('#delivery_charge_amount').val();
            var payment = $('#newPayment').val();
            var order_status = $('#order_status').val();
            var newPaymentSource = $('#newPaymentSource').val();
            if(payment == ''){
                payment = 0
            }
            if(delivery_charge_amount == ''){
                delivery_charge_amount = 0;
            }
            if(res.data.delivery_charge_status == true){
                delivery_charge_amount = 0;
            }
            if(payment > res.data.due_price){
                payment = res.data.due_price
            }
            var return_products = [];
            if(order_status == 2 || order_status == 3){
                $.each($('._order_items'),function(k,v){
                    var product_id = $(v).attr('data-id');
                    var data = {
                        product_id:product_id,
                        sku:$('#_orderPSKU_'+product_id).val(),
                        qty:parseInt($('#_orderP_'+product_id).val()),
                    }
                    return_products.push(data);
                });
            }
            var data = {
                order_id:actionOrderid,
                delivery_charge_amount:parseInt(delivery_charge_amount),
                payment:parseInt(payment),
                order_status:parseInt(order_status),
                order_uniq:res.data.order_uniq,
                company_id:res.data.company_id,
                branch_id:res.data.branch_id,
                created_by:$('#userid').val(),
                newPaymentSource:newPaymentSource,
                delivery_charge_status:res.data.delivery_charge_status,
                req_type:'updateOrder',
                order_title:$('#order_title').val(),
                customer_name:$('#customer_name').val(),
                customer_phone:$('#customer_Phone').val(),
                shipping_address:$('#shipping_address').val(),
                note:$('#orderNotes').val(),
                shipping_by: $('#shipping_by').val(),
                order_reference: $('#order_reference').val(),
                delivery_date:$('#delivery_date').val(),
                return_products:return_products
            }
            if(data.delivery_date == ''){
                data.delivery_date = res.data.delivery_date;
            }
            if(data.order_title == ''){
                openWarning('order_title');
            }else if(data.shipping_by.length < 2){
                openWarning('shipping_by');
            }else if(data.shipping_address.length < 3){
                openWarning('shipping_address');
            }else if(data.customer_phone < 10){
                openWarning('customer_Phone');
            }else if(data.customer_name < 3){
                openWarning('customer_name');
            }else if(!data.delivery_date){
                openWarning('delivery_date');
            }else{
                socket.emit('order_management',data,function(res2){
                    if(res2.status){
                        Swal.fire({
                            title: 'Success!!!',
                            text: "Successfully Order updated.",
                            type: 'success',
                            confirmButtonText: 'ok'
                        }).then((result) => {
                            location.reload();
                        })
                    }else{
                        Swal.fire({
                            title: 'Error!!!',
                            text: res2.err,
                            type: 'error',
                            confirmButtonText: 'ok'
                        }).then((result) => {
                            // location.reload();
                        });
                    }
                })
            }
        }else{
            Swal.fire({
                title: 'Error!!!',
                text: res.err,
                type: 'error',
                confirmButtonText: 'ok'
            }).then((result) => {
                // location.reload();
            });
        }
     })
}


function blurInputNumber(elm,type){
    var value = $(elm).val();
    if(type == 'orderqty'){
        if(value == ''){
            $(elm).val(1);
        }else if(value < 1){
            $(elm).val(1);
        }
        
        var product_id = $(elm).attr('data-id');
        var product_price = parseInt($(elm).attr('data-price'));
        var productQty = parseInt($(elm).val());
        var totalPrice = (product_price * productQty);
        $('#_orderP_price_'+product_id).val(totalPrice);

    }else{
        if(value == ''){
            $(elm).val(0);
        }else if(value < 0){
            $(elm).val(0);
        }
        
    }
    changeTotalPrice();
}

function statement_type_change(elm){
    if($(elm).val() == 'payment'){
        $('#payment_statement_category').show();
        $('#received_statement_category').hide();
    }else{
        $('#received_statement_category').show();
        $('#payment_statement_category').hide();
    }
}

function createNewStatement(){
    var data = {
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'createNewStatement',
        branch_id:$('#branch_id').val(),
        particulars:$('#particulars').val(),
        statement_category:$('#statement_category1').val(),
        ref:$('#statement_ref').val(),
        statement_type:$('#statement_type').val(),
        amount:$('#satementAmount').val(),
        note:$('#note_statement').val()
    }
    if(data.amount < 0){
        data.amount = 0; 
    }
    if(data.statement_type == 'payment'){
        data.statement_category = $('#statement_category2').val();
    }else{
        data.statement_category = $('#statement_category1').val();
    }
    if(data.amount == 0 || data.amount == ''){
        openWarning('amount');
    }else if(data.statement_category.length < 2){
        openWarning('statement_category');
    }else if(data.ref.length == 0){
        openWarning('ref');
    }else if(data.particulars.length < 5){
        openWarning('particulars');
    }else{
        socket.emit('statement_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Create Statement.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.err,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        })
    }
}

function createSalaryStatement(){
    var data = {
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        branch_id:$('#branch_id_salary').val(),
        user_id:$('#seleteUser').val(),
        user_name:$('#userId'+$('#seleteUser').val()).val(),
        note:$('#any_notes').val(),
        amount:parseInt($('#newPayment').val()),
        create_at:new Date(),
        statement_type:'payment',
        statement_category:'salary',
        particulars:'',
        req_type:'salary',
        ref:'',
    }

    if(data.amount <= 0){
        openWarning('amount');
    }else if(data.user_id == '' || data.user_id == null ){
        openWarning('user_id');
    }else if(data.branch_id == ''){
        openWarning('branch');
    }else{
        data.ref = data.user_id;
        data.particulars = data.user_name +' '+ data.amount +'Tk received salary from branch '+$('#branchId'+data.branch_id).val() ;
        
        socket.emit('statement_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Salary Payment.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.err,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        })
    }
}

function createInvestOrWithdraw(){
    var data = {
        company_id:$('#usercompanyid').val(),
        created_by:$('#userid').val(),
        req_type:'withdrawORinvest',
        branch_id:$('#branch_id').val(),
        particulars:$('#particulars').val(),
        statement_category:$('#statement_category1').val(),
        ref:$('#statement_ref').val(),
        statement_type:$('#statement_type').val(),
        amount:$('#satementAmount').val(),
        note:$('#note_statement').val(),
        user_id:$('#seleteUser').val(),
        user_name:''
    }
    if(data.amount < 0){
        data.amount = 0; 
    }
    
    if(data.statement_type == 'payment'){
        data.statement_category = 'withdraw';
    }else{
        data.statement_category = 'invest';
    }
    if(data.user_id == '' || data.user_id == null){
        openWarning('user_id');
    }else if(data.amount == 0 || data.amount == ''){
        openWarning('amount');
    }else if(data.statement_category.length < 2){
        openWarning('statement_category');
    }else {
        data.ref = data.user_id;
        data.user_name = $('#userId'+data.user_id).val();
        if(data.statement_category == 'withdraw'){
            data.particulars = data.user_name + ' ' + data.statement_category +' '+ data.amount +' Tk from branch'+$('#branchId'+data.branch_id).val() ;
        }else{
            data.particulars = data.user_name + ' ' + data.statement_category +' '+ data.amount +' Tk to branch'+$('#branchId'+data.branch_id).val() ;
        }
        socket.emit('statement_management',data,function(res){
            if(res.status){
                Swal.fire({
                    title: 'Success!!!',
                    text: "Successfully Create Statement.",
                    type: 'success',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    location.reload();
                })
            }else{
                Swal.fire({
                    title: 'Error!!!',
                    text: res.err,
                    type: 'error',
                    confirmButtonText: 'ok'
                }).then((result) => {
                    // location.reload();
                });
            }
        })
    }
}

function generateReport(type){
    if(type == 'order'){
        var startDate = moment($('#start_filtering_date').val()).format('YYYY-MM-DD hh:mm A');
        var endDate = moment($('#end_filtering_date').val()).format('YYYY-MM-DD')+' '+moment().format('LT');
        var orderStatus = $('#order_status_filtering').val();

        if(startDate > endDate || startDate == '' || endDate == ''){
            Swal.fire({
                title: 'Error!!!',
                text: 'Invalid Date',
                type: 'error',
                confirmButtonText: 'ok'
            });
        }else{
            var data = {
                start:startDate,
                end:endDate,
                order_status:orderStatus,
                company_id:$('#usercompanyid').val(),
                created_by:$('#userid').val(),
                req_type:'order_report',
            }

            socket.emit('order_management',data,function(res){
                console.log(res);
                if(res.status){
                    if(res.data.length == 0){
                        Swal.fire({
                            title: 'Sorry !!!',
                            text: 'No Data Found.',
                            type: 'info',
                            confirmButtonText: 'ok'
                        });
                    }else{
                        $('#reportStartAndEndDate').html('Start & End Date : '+data.start+' - '+data.end);
                        $('#reportOrdersTbody').html('');
                        $('#reportTotalOrders').html('Total Order(s) : '+res.data.length);
                        $('#createReportTime').html('Create Date : '+moment().format('DD-MM-YYYY'));
                        $.each(res.data,function(k,v){
                            var design = '<tr style="width:100%">'
                                            +'<td style="width:5%">'+(k+1)+'</td>'
                                            +'<td style="width:10%;word-break: break-word;">'+v.order_uniq+'</td>'
                                            +'<td style="width:15%;word-break: break-word;">'+moment(v.created_at).format('DD-MM-YYYY')+'</td>'
                                            +'<td style="width:15%;word-break: break-word;">'+v.order_title+'</td>'
                                            +'<td style="width:10%;word-break: break-word;">'+v.customer_name+'</td>'
                                            +'<td style="width:15%;word-break: break-word;">'+v.customer_phone+'</td>'
                                            +'<td style="width:8%;word-break: break-word;">'+v.total_price+'</td>'
                                            +'<td style="width:8%;word-break: break-word;">'+v.payment+'</td>'
                                            +'<td style="width:14%;word-break: break-word;">'+getOrderStatus(v.order_status)+'</td>'
                                        +'</tr>';
                            $('#reportOrdersTbody').append(design)
                        });
                        // var doc = new jsPDF('p', 'pt', 'letter');
                        // doc.fromHTML($('#downloadabletable').html(),15,15, {
                        //         'elementHandlers': specialElementHandlers
                        // });
                        // doc.save($('#usercompanyname').val()+'@ '+$('#reportStartAndEndDate').html()+'.pdf');
                        var divToPrint=$('#downloadabletable');
                            divToPrint.find('table').css({
                                'border': 'solid 1px #DDEEEE',
                                'border-collapse': 'collapse',
                                'border-spacing': '0',
                                'font': 'normal 13px Arial, sans-serif'
                            });
                            divToPrint.find('#companyNameOrder').css({
                                'font': 'normal 20px Arial, sans-serif',
                                'text-align':'center'
                            });
                            divToPrint.find('.reportp').css({
                                'font': 'normal 12px Arial, sans-serif',
                                'text-align':'center'
                            });
                            divToPrint.find('td').css({
                                'border': 'solid 1px #DDEEEE',
                                'color': '#333',
                                'padding': '10px',
                                'text-shadow': '1px 1px 1px #fff',
                            });
                            divToPrint.find('th').css({
                                'background-color': '#DDEFEF',
                                'border': 'solid 1px #DDEEEE',
                                'color': '#336B6B',
                                'padding': '10px',
                                'text-align': 'center',
                                'text-shadow': '1px 1px 1px #fff'
                            });
                            newWin = window.open("");
                            newWin.document.write(divToPrint.html());
                            newWin.print();
                            newWin.close();
                    }
                }else{
                    Swal.fire({
                        title: 'Error!!!',
                        text: 'Something Wrong!!!',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                }
            })
        }
    }else if(type == 'statement'){
        var startDate = moment($('#start_filtering_date').val()).format('YYYY-MM-DD hh:mm A');
        var endDate = moment($('#end_filtering_date').val()).format('YYYY-MM-DD')+' '+moment().format('LT');
        var statement_type = $('#order_status_filtering').val();

        if(startDate > endDate || startDate == '' || endDate == ''){
            Swal.fire({
                title: 'Error!!!',
                text: 'Invalid Date',
                type: 'error',
                confirmButtonText: 'ok'
            });
        }else{
            var data = {
                start:startDate,
                end:endDate,
                statement_type:statement_type,
                company_id:$('#usercompanyid').val(),
                created_by:$('#userid').val(),
                req_type:'report_with_date',
            }

            socket.emit('statement_management',data,function(res){
                console.log(res);
                if(res.status){
                    if(res.data.length == 0){
                        Swal.fire({
                            title: 'Sorry !!!',
                            text: 'No Data Found.',
                            type: 'info',
                            confirmButtonText: 'ok'
                        });
                    }else{
                        $('#reportStartAndEndDate').html('Start & End Date : '+data.start+' - '+data.end);
                        $('#reportOrdersTbody').html('');
                        $('#createReportTime').html('Create Date : '+moment().format('DD-MM-YYYY'));
                        var totalReceived = 0;
                        var totalExpense = 0;
                        $.each(res.data,function(k,v){
                            if(v.statement_type == 'received'){
                                totalReceived = (totalReceived + v.amount);
                            }else{
                                totalExpense = (totalExpense + v.amount);
                            }
                            var iconS = ''
                            if(v.statement_type == 'received'){
                                iconS = '(+) '+(v.amount)+'Tk';   
                            }else{
                                iconS = '(-) '+(v.amount)+'Tk';     
                            }
                            var design = '<tr>'
                                            +'<td style="width:5%">'+(k+1)+'</td>'   
                                            +'<td style="width:10%">'+v.statement_uniq+'</td>'   
                                            +'<td style="width:15%">'+moment(v.created_at).format('DD-MM-YYYY')+'</td>'   
                                            +'<td style="width:30%">'+v.particulars+'</td>'   
                                            +'<td style="width:20%">'+v.statement_category+'</td>'   
                                            +'<td style="width:20%">'+iconS+'</td>'   
                                         +'</tr>'
                            $('#reportOrdersTbody').append(design);
                        });
                        $('#totalReceivedAmount').html((totalReceived)+'Tk');
                        $('#totalExpenseAmount').html((totalExpense)+'Tk');
                        $('#GrandTotalForStatement').html((totalReceived - totalExpense) +'Tk');
                        // var doc = new jsPDF('p', 'pt', 'letter');
                        // doc.fromHTML($('#downloadabletable').html(),15,15, {
                        //         'elementHandlers': specialElementHandlers
                        // });
                        // doc.save($('#usercompanyname').val()+'@ '+$('#reportStartAndEndDate').html()+'.pdf');
                        var divToPrint=$('#downloadabletable');
                            divToPrint.find('table').css({
                                'border': 'solid 1px #DDEEEE',
                                'border-collapse': 'collapse',
                                'border-spacing': '0',
                                'font': 'normal 13px Arial, sans-serif'
                            });
                            divToPrint.find('#companyNameOrder').css({
                                'font': 'normal 20px Arial, sans-serif',
                                'text-align':'center'
                            });
                            divToPrint.find('.reportp').css({
                                'font': 'normal 12px Arial, sans-serif',
                                'text-align':'center'
                            });
                            divToPrint.find('td').css({
                                'border': 'solid 1px #DDEEEE',
                                'color': '#333',
                                'padding': '10px',
                                'text-shadow': '1px 1px 1px #fff'
                            });
                            divToPrint.find('th').css({
                                'background-color': '#DDEFEF',
                                'border': 'solid 1px #DDEEEE',
                                'color': '#336B6B',
                                'padding': '10px',
                                'text-align': 'center',
                                'text-shadow': '1px 1px 1px #fff'
                            });
                            newWin = window.open("");
                            newWin.document.write(divToPrint.html());
                            newWin.print();
                            newWin.close();
                    }
                }else{
                    Swal.fire({
                        title: 'Error!!!',
                        text: 'Something Wrong!!!',
                        type: 'error',
                        confirmButtonText: 'ok'
                    });
                }
            })
        }
    }
}



var getOrderStatus = (type)=>{
    switch (type) {
      case 0:
          return "INITIATED";
      case 1:
          return "DELIVERED";
      case 2:
          return "RETURN";
      case 3:
          return "CANCELED";
      case 4:
          return "PENDDING";
      default:
          return "INITIATED";
    }
  }