const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const customer_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    customer_id:{
        type:String,
        required:true
    },
    customer_name:{
        type:String,
        required:true
    },
    customer_phone:{
        type:String,
        required:true
    },
    customer_email:{
        type:String,
        default:null
    },
    customer_address:{
        type:String,
        default:null
    },
    customer_shipping_address:{
        type:String,
        default:null
    },
    customer_img:{
        type:String,
        default:null
    },
    customer_status:{
        type:Boolean,
        default:true
    },
    customer_priority:{
        type:String,
        default:null
    },
    total_order:{
        type:Number,
        default:0
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

customer_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('customer',customer_schema);