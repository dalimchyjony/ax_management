const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const transaction_sechema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    transaction_id:{
        type:String,
        required:true
    },
    transaction_type:{
        type:String,
        required:true
    },
    invoice_id:{
        type:String,
        required:true
    },
    transaction_amount:{
        type:Number,
        default:0
    },
    old_balance:{
        type:Number,
        required:true
    },
    new_balance:{
        type:Number,
        required:true
    },
    account_id:{
        type:String,
        required:true
    },
    account_holder_name:{
        type:String,
        required:true
    },
    transaction_status:{
        type:Boolean,
        default:true
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

transaction_sechema.plugin(mongoosePaginate);

module.exports = mongoose.model('transaction',transaction_sechema);