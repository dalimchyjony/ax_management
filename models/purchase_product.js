const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const purchase_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    purchase_id:{
        type:String,
        required:true
    },
    purchase_uniq:{
        type:String,
        required:true
    },
    product_id:{
        type:String,
        required:true
    },
    sku:{
        type:String,
        required:true
    },
    branch_id:{
        type:String,
        required:true
    },
    supplier_id:{
        type:String,
        required:true
    },
    ref:{
        type:String,
        default:null
    },
    total_amount:{
        type:Number,
        default:0
    },
    qty:{
        type:Number,
        default:0
    },
    notes:{
        type:String,
        default:null
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

purchase_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('purchase_product',purchase_schema);