const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const account_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    account_id:{
        type:String,
        required:true
    },
    account_name:{
        type:String,
        required:true
    },
    account_holder_name:{
        type:String,
        required:true
    },
    account_type:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    email:{
        type:String,
        default:null
    },
    address:{
        type:String,
        default:null
    },
    nid:{
        type:String,
        default:null
    },
    balance:{
        type:Number,
        default:0
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

account_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('account',account_schema);