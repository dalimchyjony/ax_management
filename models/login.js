const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const login_schema = new Schema({
    user_id:{
        type:String,
        required:true
    },
    company_id:{
        type:String,
        required:true
    },
    user_phone:{
        type:String,
        required:true
    },
    user_email:{
        type:String,
        default:null
    },
    password:{
        type:String,
        default:null
    },
    status:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

login_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('login',login_schema);