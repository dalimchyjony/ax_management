const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const category_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    category_id:{
        type:String,
        required:true
    },
    category_name:{
        type:String,
        required:true
    },
    parent_category_id:{
        type:String,
        default:null
    },
    category_slug:{
        type:String,
        required:true
    },
    category_img_id:{
        type:String,
        default:null
    },
    category_status:{
        type:Boolean,
        default:true
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

category_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('category',category_schema);