const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const files_schema = new Schema({
    file_name:{
        type:String,
        required:true
    },
    company_id:{
        type:String,
        required:true
    },
    file_id:{
        type:String,
        required:true
    },
    file_type:{
        type:String,
        default:null
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

files_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('files',files_schema);