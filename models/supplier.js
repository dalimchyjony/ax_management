const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const supplier_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    supplier_id:{
        type:String,
        required:true
    },
    supplier_name:{
        type:String,
        required:true
    },
    supplier_phone:{
        type:String,
        required:true
    },
    supplier_address:{
        type:String,
        default:null
    },
    supplier_email:{
        type:String,
        default:null
    },
    total_amount:{
        type:Number,
        default:0
    },
    receivable_amount:{
        type:Number,
        default:0
    },
    payable_amount:{
        type:Number,
        default:0
    },
    supplier_priority:{
        type:String,
        default:null
    },
    supplier_status:{
        type:Boolean,
        default:true
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

supplier_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('supplier',supplier_schema);