const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const orders_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    order_id:{
        type:String,
        required:true
    },
    order_uniq:{
        type:String,
        required:true
    },
    order_title:{
        type:String,
        default:null
    },
    branch_id:{
        type:String,
        required:true
    },
    products:{
        type:Array,
        default:[]
    },
    return_products:{
        type:Array,
        default:[]
    },
    customer_name:{
        type:String,
        required:true
    },
    customer_phone:{
        type:String,
        required:true
    },
    shipping_by:{
        type:String,
        required:true
    },
    shipping_address:{
        type:String,
        required:true
    },
    delivery_charge:{
        type:Number,
        default:0
    },
    vat_tax:{
        type:Number,
        default:0
    },
    discount_amount:{
        type:Number,
        default:0
    },
    payment:{
        type:Number,
        default:0
    },
    due_price:{
        type:Number,
        default:0
    },
    total_price:{
        type:Number,
        default:0
    },
    total_product_price:{
        type:Number,
        default:0
    },
    total_product_purchase_price:{
        type:Number,
        default:0
    },
    order_total_cost:{
        type:Number,
        default:0
    },
    order_date:{
        type:Date,
        default:Date.now
    },
    delivery_date:{
        type:Date,
        default:Date.now
    },
    note:{
        type:String,
        default:null
    },
    customer_feedback:{
        type:String,
        default:null
    },
    order_reference:{
        type:String,
        default:null
    },
    order_status:{
        type:Number,
        default:0
    },
    delivery_charge_status:{
        type:Boolean,
        default:false
    },
    delivery_charge_amount:{
        type:Number,
        default:0
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

orders_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('orders',orders_schema);