const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const company_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    company_name:{
        type:String,
        required:true
    },
    company_phone:{
        type:String,
        required:true
    },
    status:{
        type:Boolean,
        default:true
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

company_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('company',company_schema);