const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const statement_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    branch_id:{
        type:String,
        required:true
    },
    statement_id:{
        type:String,
        required:true
    },
    statement_uniq:{
        type:String,
        required:true
    },
    particulars:{
        type:String,
        required:true
    },
    statement_category:{
        type:String,
        required:true
    },
    ref:{
        type:String,
        required:true
    },
    statement_type:{ 
        type:String,
        required:true
    },
    amount:{
        type:Number,
        default:0
    },
    note:{
        type:String,
        default:null
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

statement_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('statement',statement_schema);