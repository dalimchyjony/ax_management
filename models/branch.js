const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');


//create schema
const branch_schema = new Schema({
    company_id:{
        type:String,
        required:true
    },
    branch_id:{
        type:String,
        required:true
    },
    branch_name:{
        type:String,
        required:true
    },
    branch_address:{
        type:String,
        default:null
    },
    branch_email:{
        type:String,
        default:null
    },
    branch_phone:{
        type:String,
        required:true
    },
    branch_status:{
        type:Boolean,
        default:true
    },
    current_balance:{
        type:Number,
        default:0
    },
    receivable_amount:{
        type:Number,
        default:0
    },
    payable_amount:{
        type:Number,
        default:0
    },
    created_by:{
        type:String,
        required:true
    },
    created_at:{
        type:Date,
        default:Date.now
    }
});

branch_schema.plugin(mongoosePaginate);

module.exports = mongoose.model('branch',branch_schema);