var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var moment = require('moment');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const mongoose = require('mongoose');
var ejs = require('ejs');
const passport = require('passport');
const flash = require('connect-flash');
var socketIO = require('socket.io');
var sharedsession = require("express-socket.io-session");
const secret = require('./config/keys').secret;
const compression = require('compression');
let oneYear = 1 * 365 * 24 * 60 * 60 * 1000;
var session = require('express-session')({
    secret: secret,
    resave: true,
    saveUninitialized: true
  });
var mkdirp = require('mkdirp');
global.mkdirp = mkdirp; 
  
utilsLogin = require('./utils/login');
utilsCompany = require('./utils/company');
utilsSupplier = require('./utils/supplier');
utilsBranch = require('./utils/branch');
utilsUser = require('./utils/user');
utilsCustomer = require('./utils/customer');
utilsCategory = require('./utils/category');
utilsProduct = require('./utils/product');
utilsPurchaseProduct = require('./utils/purchase_product');
utilsFiles = require('./utils/files');
utilsStatement = require('./utils/statement');
utilsOrders = require('./utils/orders');

restart_time = moment().unix();
// Passport Config
require('./config/passport')(passport);

//DB config
const db = require('./config/keys').mongoURI;

//Mongo DB connection
mongoose.connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true 
  }).then(()=>{
    console.log("DB connected");
  }).catch((err)=>{
    console.log("DB Error",err);
  });

var index = require('./routes/index');
var management = require('./routes/management');
var reactapi = require('./routes/api');

var app = express();
// compress all responses
app.use(compression());

// Caches the static files for a year.
app.use('/', express.static(__dirname + '/public/', { maxAge: oneYear }));
app.use('/', express.static(__dirname + '/view/', { maxAge: oneYear }));

alluserlist = [];



  app.use(session);
  
  // Passport middleware
  app.use(passport.initialize());
  app.use(passport.session());
  
  // Connect flash
  app.use(flash());
  
  // Global variables
  app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
 

  // Socket.io
  var io = socketIO();
  app.io = io;
  io.use(sharedsession(session, {
      autoSave: true
  }));
  io.of('/namespace').use(sharedsession(session, {
      autoSave: true
  }));
  
  require('./socket/socket.js')(io);
  
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/ax-management', management);
app.use('/api', reactapi);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
